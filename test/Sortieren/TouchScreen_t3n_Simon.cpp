#include <Arduino.h>

// Touchscreen
#include <Adafruit_GFX.h>
// #include <myFonts/font_Michroma.h>
// #include <ili9341_t3n_font2font_Michroma.h>
// #include "font_Arial.h"
// #include "font_ArialBold.h"
// #include <Fonts/Org_01.h>
// #include <Fonts/FreeSerif9pt7b.h>
// #include <fonts/FreeSans12pt7b.h>
// #include <Fonts/FreeMono9pt7b.h>
// #include <Fonts/FreeSans9pt7b.h>
// #include <ILI9341_fonts.h>
#include <Wire.h>
#include <ili9341_t3n_font_Arial.h>
#include <ili9341_t3n_font_ArialBold.h>
#include <ili9341_t3n_font_ComicSansMS.h>
//#include <ili9341_t3n_font_Michroma.h>
#include <ili9341_t3n_font_OpenSans.h>
#include "ILI9341_t3n.h"
#include "SPI.h"

#define TFT_DC 9
#define TFT_CS 10
#define TFT_RST 8
#define CENTER ILI9341_t3n::CENTER
DMAMEM uint16_t fb1[320 * 240];
DMAMEM uint16_t fb2[320 * 240];

ILI9341_t3n tft = ILI9341_t3n(TFT_CS, TFT_DC, TFT_RST);

// RTC
#include "RTClib.h"
RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sonntag",    "Montag",  "Dienstag", "Mittwoch",
                             "Donnerstag", "Freitag", "Samstag"};

void waitUserInput() {
    Serial.println("Press any key to continue");
    while(Serial.read() == -1) {};
    while(Serial.read() != -1) {};
    Serial.println("Continued");
}

void setup() {
    Serial.begin(115200);
    // while(!Serial) {};
    if(!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        abort();
    }
    if(rtc.lostPower()) {
        Serial.println("RTC lost power, let's set the time!");
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    }
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    Serial.println("Zeit gesetzt");

    delay(500);
    tft.begin();
    tft.setRotation(1);
    tft.setTextSize(2);
    // NOTE Schriftart
    // tft.setFont(OpenSans20);
    // tft.setFont(ComicSansMS_20);
    // tft.setFont(&FreeSans9pt7b); // Für GFX Library fonts. Sind aber stark verpixelt
    tft.setFont(Arial_12);
    tft.useFrameBuffer(fb1);
    tft.setFrameBuffer(fb1);
    // http://www.barth-dev.de/online/rgb565-color-picker/
    tft.fillScreen(0x39E7);
}

int x1 = 20;
int x2 = 120;
int y = 20;

void loop() {
    // tft.setTextColor(0x1C40, 0x39E7);
    // tft.setTextColor(ILI9341_LIGHTGREY, ILI9341_DARKGREY);
    tft.setTextColor(ILI9341_WHITE);
    tft.fillRectVGradient(10, 10, 260, 75, ILI9341_BLACK, ILI9341_DARKGREY);
    // tft.fillScreen(ILI9341_DARKGREY);

    DateTime now = rtc.now();

    // Datum
    char dateBuffer[12];
    sprintf(dateBuffer, "%02u.%02u.%04u ", now.day(), now.month(), now.year());
    tft.setCursor(x1, y);
    tft.print("Datum");
    tft.setCursor(x2, y);
    tft.print(dateBuffer);
    // Zeit
    tft.setCursor(x1, y + 20);
    sprintf(dateBuffer, "%02u:%02u:%02u ", now.hour(), now.minute(), now.second());
    tft.print("Zeit");
    tft.setCursor(x2, y + 20);
    tft.print(dateBuffer);
    // Temperatur start
    tft.setCursor(x1, y + 40);
    tft.print("Temp.");
    tft.setCursor(x2, y + 40);
    tft.print(rtc.getTemperature());
    tft.print((char)247);
    tft.setFont(Arial_12);
    // Temp Grad verkleinern
    tft.setCursor(x2 + 73, y + 35);
    tft.print("o");
    tft.setFont(Arial_12);
    // Temp Celsius schreiben
    tft.setCursor(x2 + 83, y + 40);
    tft.print("C");
    tft.updateScreenAsync();
    tft.waitUpdateAsyncComplete();
    // tft.drawRect(10, 10, 250, 75, ILI9341_BLUE); // x, y, w, h, color
    // tft.drawLine(105, y, 105, y + 40, ILI9341_WHITE); // x0, y0, x1, y1, color
    tft.drawLine(105, 20, 105, 72, ILI9341_WHITE); // x0, y0, x1, y1, color
}