// Gartenhaus
#include <Arduino.h>
#include <Adafruit_GFX.h> // Core graphics library
#include <MCUFRIEND_kbv.h> // Hardware-specific library
#include <SPI.h>
#include <Wire.h>
// #include <ILI9341_t3n.h>

MCUFRIEND_kbv tft;

#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSerif12pt7b.h>

#include <FreeDefaultFonts.h>

#define BLACK 0x0000
#define RED 0xF800
#define GREEN 0x07E0
#define WHITE 0xFFFF
#define GREY 0x8410
#define NAVY 0x000F
#define DARKGREEN 0x03E0
#define DARKCYAN 0x03EF
#define MAROON 0x7800
#define PURPLE 0x780F
#define OLIVE 0x7BE0
#define LIGHTGREY 0xC618
#define DARKGREY 0x7BEF
#define BLUE 0x001F
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define ORANGE 0xFD20
#define GREENYELLOW 0xAFE5
#define PINK 0xF81F

#define TESTPIN 32
#define RS3485RTS 18
#define RESET 24
//#define TFT_DC  9
//#define TFT_CS 10
//#define TFT_RST 8
//#define TFT_RW  7
//#define TFT_SS  6

// Defs for incoming String

byte Year;
byte Month;
byte Date;
byte DoW;
byte Hour;
byte Minute;
byte Second;

boolean mainStart = false;

int x = 0;

// ========================================================================================

void showmsgXY(int x, int y, int sz, const GFXfont *f, const char *msg) {
    int16_t x1, y1;
    uint16_t wid, ht;
    // tft.drawFastHLine(0, y, tft.width(), WHITE);
    tft.setFont(f);
    tft.setCursor(x, y);
    tft.setTextColor(GREEN);
    tft.setTextSize(sz);
    tft.print(msg);
    delay(1000);
}

// ========================================================================================

void setup(void) {

    pinMode(TESTPIN, OUTPUT);
    pinMode(RS3485RTS,OUTPUT);
    pinMode(RESET, OUTPUT);


    // pinMode(TFT_DC, OUTPUT);
    // pinMode(TFT_CS, OUTPUT);
    // pinMode(TFT_RST, OUTPUT);
    // pinMode(TFT_RW, OUTPUT);
    // pinMode(TFT_SS, OUTPUT);

    // digitalWrite(TFT_RW, LOW);
    // digitalWrite(TFT_SS, HIGH);
    //digitalWrite(TFT_RST, LOW);
    digitalWrite(RS3485RTS, LOW);
    digitalWrite(RESET, LOW);
    delay(20);
    digitalWrite(RESET, HIGH);
 
    Serial.begin(115200);
    Serial4.begin(115200);

    delay(1000);
    // while (!Serial) {};

    // Serial.println("Please enter Test-String: "); // Prompt User for input
    while(Serial.available() == 0) {}
    uint16_t ID = tft.readID();

    Serial.println("Example: Font_simple");
    Serial.print("found ID = 0x");
    Serial.println(ID, HEX);
    if (ID == 0xD3D3) ID = 0x9481; // force ID if write-only display    
    
    tft.begin(ID);
    tft.setRotation(1);
    tft.fillScreen(BLACK);

    Serial.println(ID, HEX);
    /*
        do  {           // test with
        infinity loop digitalWrite(TESTPIN,HIGH); showmsgXY(0, 25, 1,
        &FreeSans12pt7b, "abcdef"); delayMicroseconds(10);
        digitalWrite(TESTPIN,LOW);
        delayMicroseconds(10);
        } while (x < 10);
    */

    delay(1000);
    showmsgXY(0, 25, 1, &FreeSans12pt7b, "Tag Nr. des lfd. Jahres:");
    showmsgXY(0, 50, 1, &FreeSans12pt7b, "Mitteleurop.Zeit  (MEZ):");
    showmsgXY(0, 75, 1, &FreeSans12pt7b, "Wahre Ortszeit  (WOZ):");
    showmsgXY(0, 100, 1, &FreeSans12pt7b, "Mittlere Ortszeit (MOZ):");
    showmsgXY(0, 125, 1, &FreeSans12pt7b, "Zeitgleichung :");
    showmsgXY(0, 150, 1, &FreeSans12pt7b, "Sonnenaufgang :");
    //  tft.setTextColor(RED, GREY);
    showmsgXY(0, 175, 1, &FreeSans12pt7b, "Sonnenuntergang");
    showmsgXY(0, 200, 1, &FreeSans12pt7b, "Tageslaenge:");
    showmsgXY(0, 225, 1, &FreeSans12pt7b, "Deklination:");
    showmsgXY(0, 250, 1, &FreeSans12pt7b, "Azimut :");
    showmsgXY(0, 275, 1, &FreeSans12pt7b, "Elevation :");
    showmsgXY(0, 300, 1, &FreeSans12pt7b, "Refraktion :");
    showmsgXY(280, 50, 1, &FreeSans12pt7b, "10:58:12 h");
    // showmsgXY(280, 50, 1, &FreeSans12pt7b, ID);
}

// ==================================================================================================

void loop(void) {

    // unsigned long currentMillis = millis();
    int incomingByte = 0;
    boolean incomingMSG = false;
    int j;
    byte stringLength;
    
    //================       Begin read incoming string     ===============
    boolean GotString = false;
    boolean getMSG = false;

    char InChar;

    byte(Temp0);
    byte(Temp1);
 //   byte(Temp2);
//    byte(Temp3);


    // NOTE Definition InString
    char InString[20];
    char InString2[20];
    char msgSelect;
/*
    while(!incomingMSG) {
        if(Serial4.available()) {
            InChar = Serial4.read();
            if(InChar == 'z') {         // Z = Leading Char
                incomingMSG = true;
                Serial.println("   Message  z =  ");
                Serial.println(InString[0]);
            }
        }
    }
*/
/*
    boolean getMSG =false;
    int stringLength = 0;
    int j = 0;
    GotString = false;

        while(!GotString) {
        if(Serial4.available()) {
            InChar = Serial4.read();
            if  (InChar == 'z') {         // Z = Leading Char
                InString[j] = InChar;
                getMSG= true;
                }
            //  Abfrage : ist InChar = z ?
            if (getMSG) {
                j ++;
                if(InChar == 'x') { // x = Stop Message Char
                    GotString = true;
                    Serial.print("   Message  z =  ");
                    Serial.println(InString);
                    // while(!Serial.available());
                    delay(2000);
                    }
            }
        }
    }
*/


    while(!GotString) {
        if(Serial4.available()) {
            InChar = Serial4.read();

            if (getMSG = false) {

                if (InChar == 'z') {
                    getMSG = true;
                    InString[j] = InChar;
                    j ++ ;
                }
            else {
                InString[j] = InChar;
                if (InChar == '\n' )  {
                    GotString = true;
                    Serial.print("   Message  z =  ");
                    Serial.println(InString);
                    }
                else {  j ++ ;
                    }
                }
            }
        }
    }


    // Read Type  first

    msgSelect = InString[1];
    Serial.print("MSG = ");
    Serial.println(msgSelect);

   //Calculate leghth message

    Temp0 = (byte)InString[2] - 48;
    Temp1 = (byte)InString[3] - 48;
    stringLength = Temp0 * 10 + Temp1;


/*
    while(!Serial.available());
    delay(500);
    // msgSelect = i??????;   ////  asci to dec
*/
// NOTE Funktioniert nicht


/*

for (int i = 2; i < sizeof(InString) - 1; i++) {
    // Serial.print(i, DEC);
    // Serial.print(" = ");
    Serial.write(InString[i]);    
    if (i > 1) {
        // Serial.println
        // Serial.println(InString[i]);        
        if (InString[i] != char("x")) {
            InString2[j] = InString[i];
            //Serial.println("XXXXX Gefunden XXXXX");
            j++;
        }
    }
}
*/

    Serial.print("StringLength  = ");
    Serial.println(stringLength);

    j = 0;
    for ( int i = 0; i  < 14; i++) {
    // Serial.print(i, DEC);
    // Serial.print(" = ");
        if (i > 3) {
            Serial.write(InString2[j]= InString[i]);
            j++;
        }
    }




// NOTE neuer Versuch
//char NewString[];
// NewString = InString.substr(0, InString.size()-1); <-- Neuer Versuch
//NewString = InString.substring(2, (i-1));
    Serial.println();
    Serial.print("String  =");
    Serial.println(InString2);

    // Read Year Second
    // Temp0 = (byte)InString[0] - 48;
/*  Temp1 = (byte)InString[2] - 48;
    Temp2 = (byte)InString[3] - 48;
    Year = Temp1 * 10 + Temp2;
    Temp1 = (byte)InString[5] - 48;
    Temp2 = (byte)InString[6] - 48;
    Month = Temp1 * 10 + Temp2;
    Temp1 = (byte)InString[8] - 48;
    Temp2 = (byte)InString[9] - 48;
    Date = Temp1 * 10 + Temp2;
*/
    //Serial.println(Year);
    //Serial.println(msgSelect);

    // ===============      End Incoming String     ========================
    /**
        if(Serial.available()) { // if there is data comming
            // String command = Serial.readStringUntil('\n'); // read string
            until
            // meet newline character
            incomingByte = Serial.read();
            // say what you got:
            Serial.print("I received: ");
            Serial.println(incomingByte, DEC);
        }
    */
    // tft.setCursor(x,y); //setCursor(int16_t x, int16_t y)  // This code sets
    // the cursor position to of x and y

    //    a  b  c   d   e   f   g   h   i   j   k  l
    //   97 98 99 100 101 102 103 104 105 106 107 108

    switch(msgSelect) {
        case 'a': // Taste "a" = Tag Nr. des lfd. Jahres
        // Serial.println(Year);
            tft.writeFillRect(279, 0, 279, 25, BLACK);
            // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
            tft.setCursor(280, 25);
            //tft.print(Year);
            //tft.print(Month);
            //tft.print(Date);
            tft.print(InString2);
            delay(5000);  // slow down the program
            // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  0");
            break;
        case 'b': // Taste "b" = MEZ Mittel Europ. Zeit
            tft.writeFillRect(279, 50, 279, 75, BLACK);
            showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
            break;
        case 'c': // Taste "c" = WOZ Wahre Ortszeit
            tft.writeFillRect(279, 50, 279, 75, BLACK);
            showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  2");
            break;
        case 'd': // Taste "d" = Mittlere Ortszeit
            tft.writeFillRect(279, 50, 279, 75, BLACK);
            showmsgXY(280, 75, 1, &FreeSans12pt7b, "       ");
            break;
        default:
            // Tue etwas, im Defaultfall
            // Dieser Fall ist optional
            break; // Wird nicht benötigt, wenn Statement(s) vorhanden sind
    }

    // =============  Der unteren Abschnitt wird nur einmal im Setup benötigt
    // =========================

    // tft.updateScreenAsync();
    // tft.waitUpdateAsyncComplete();
    // tft.drawRect(10, 10, 250, 75, ILI9341_BLUE); // x, y, w, h, color
    // tft.drawLine(105, y, 105, y + 40, ILI9341_WHITE); // x0, y0, x1, y1,
    // color

    // tft.drawLine(280, 50, 280, 75, WHITE); // x0, y0, x1, y1, color

    // tft.fillScreen(BLACK);
    // showmsgXY(20, 10, 1, NULL, "System x1");

    // showmsgXY(50, 60, 1, &FreeSans9pt7b, "FreeSans9pt7b");
    // showmsgXY(20, 80, 1, &FreeSans12pt7b, "FreeSans12pt7b");
    // showmsgXY(20, 100, 1, &FreeSerif12pt7b, "FreeSerif12pt7b");
    // showmsgXY(20, 120, 1, &FreeSmallFont, "FreeSmallFont");
    // showmsgXY(5, 180, 1, &FreeSevenSegNumFont, "01234");
    // showmsgXY(5, 190, 1, NULL, "System Font is drawn from topline");
    // tft.setTextColor(RED, GREY);
    // tft.setTextSize(2);
    // tft.setCursor(0, 220);
    // tft.print("7x5 can overwrite");
    // tft.print("7x5 can overwrite");
    // delay(1000);
    // tft.setCursor(0, 220);
    // tft.print("if background set");
    // delay(1000);
    // showmsgXY(5, 260, 1, &FreeSans9pt7b, "Free Fonts from baseline");
    // showmsgXY(5, 285, 1, &FreeSans9pt7b, "Free Fonts transparent");
    // delay(1000);
    // showmsgXY(5, 285, 1, &FreeSans9pt7b, "Free Fonts XXX");
    // delay(1000);
    // showmsgXY(5, 310, 1, &FreeSans9pt7b, "erase backgnd with fillRect()");
    delay(50);
}