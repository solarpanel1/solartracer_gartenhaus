#include <Adafruit_GFX.h> // Core graphics library
#include <Arduino.h>
#include <MCUFRIEND_kbv.h> // Hardware-specific library
#include <SPI.h>
#include <Wire.h>
#define LED_Pin1 29
#define LED_Pin2 30
#define LED_Pin3 31
#define LED_Pin4 32
#define LED_Pin5 41

void setup() {
    pinMode(LED_Pin1, OUTPUT);
    pinMode(LED_Pin2, OUTPUT);
    pinMode(LED_Pin3, OUTPUT);
    pinMode(LED_Pin4, OUTPUT);
    pinMode(LED_Pin5, OUTPUT);
    Serial.begin(115200);
    digitalWrite(LED_Pin4, HIGH);
    Serial.println("Serial funktioniert im Setup");
    delay(5000);
    Serial.println("Weiter geht's");
}

void loop() {
    Serial.println("Serial funktioniert im Loop");
    digitalWrite(LED_Pin1, HIGH);
    digitalWrite(LED_Pin2, HIGH);
    digitalWrite(LED_Pin3, HIGH);
    digitalWrite(LED_Pin4, HIGH);
    digitalWrite(LED_Pin5, HIGH);
    delay(500);
    digitalWrite(LED_Pin1, LOW);
    digitalWrite(LED_Pin2, LOW);
    digitalWrite(LED_Pin3, LOW);
    digitalWrite(LED_Pin4, LOW);
    digitalWrite(LED_Pin5, LOW);
    delay(500);
}