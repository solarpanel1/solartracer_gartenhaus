// Gartenhaus
#include <Adafruit_GFX.h> // Core graphics library
#include <Arduino.h>
#include <MCUFRIEND_kbv.h> // Hardware-specific library
#include <SPI.h>
#include <Wire.h>

#define HwSerial Serial4
MCUFRIEND_kbv tft;
const unsigned int MaxMessageLength = 20;

#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSerif12pt7b.h>
#include <FreeDefaultFonts.h>

#define BLACK 0x0000
#define RED 0xF800
#define GREEN 0x07E0
#define WHITE 0xFFFF
#define GREY 0x8410
#define NAVY 0x000F
#define DARKGREEN 0x03E0
#define DARKCYAN 0x03EF
#define MAROON 0x7800
#define PURPLE 0x780F
#define OLIVE 0x7BE0
#define LIGHTGREY 0xC618
#define DARKGREY 0x7BEF
#define BLUE 0x001F
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define ORANGE 0xFD20
#define GREENYELLOW 0xAFE5
#define PINK 0xF81F

#define TESTPIN 32
//#define RS3485RTS 18
//#define RESET 24
//#define TFT_DC  9
//#define TFT_CS 10
//#define TFT_RST 8
//#define TFT_RW  7
//#define TFT_SS  6

// Defs for incoming String

byte Year;
byte Month;
byte Date;
byte DoW;
byte Hour;
byte Minute;
byte Second;

boolean mainStart = false;
boolean GotString = false;
boolean getMSG = false;

int stringLength = 0;

byte(Temp0);
byte(Temp1);
// byte stringLength;
char InChar;
char InString[20];
char InString2[20];
char msgSelect;

String incomingString;
int j = 0;
int x = 0;
int count = 0;
int LoopCount = 0;
int LoopPos = 0;
// ========================================================================================

void showmsgXY(int x, int y, int sz, const GFXfont *f, const char *msg) {
    // int16_t x1, y1;
    // uint16_t wid, ht;
    // tft.drawFastHLine(0, y, tft.width(), WHITE);
    tft.setFont(f);
    tft.setCursor(x, y);
    tft.setTextColor(GREEN);
    tft.setTextSize(sz);
    tft.print(msg);
    delay(100);
}

// ========================================================================================

void setup() {
    pinMode(TESTPIN, OUTPUT);
    // pinMode(RS3485RTS,OUTPUT);
    // pinMode(RESET, OUTPUT);

    // pinMode(TFT_DC, OUTPUT);
    // pinMode(TFT_CS, OUTPUT);
    // pinMode(TFT_RST, OUTPUT);
    // pinMode(TFT_RW, OUTPUT);
    // pinMode(TFT_SS, OUTPUT);

    // digitalWrite(TFT_RW, LOW);
    // digitalWrite(TFT_SS, HIGH);
    // digitalWrite(TFT_RST, LOW);
    // digitalWrite(RS3485RTS, LOW);
    // digitalWrite(RESET, LOW);
    // delay(20);
    // digitalWrite(RESET, HIGH);

    // while (!Serial && millis() < 4000);
    Serial.begin(115200);
    HwSerial.begin(115200);

    // while(!Serial) {};
    // if(!rtc.begin()) {
    //     Serial.println("Couldn't find RTC");
    //     Serial.flush();
    //     abort();

    // Serial.println("Please enter Test-String: "); // Prompt User for
    // input while(Serial.available() == 0) {};

    uint16_t ID = tft.readID();
    Serial.println("Example: Font_simple");
    Serial.print("found ID = 0x");
    Serial.println(ID, HEX);
    if(ID == 0xD3D3)
        ID = 0x9481; // force ID if write-only display

    tft.begin(ID);
    tft.setRotation(1);
    tft.fillScreen(BLACK);

    showmsgXY(0, 25, 1, &FreeSans12pt7b, "Tag Nr. des lfd. Jahres:");
    showmsgXY(0, 50, 1, &FreeSans12pt7b, "Mitteleurop.Zeit  (MEZ):");
    showmsgXY(0, 75, 1, &FreeSans12pt7b, "Wahre Ortszeit  (WOZ):");
    showmsgXY(0, 100, 1, &FreeSans12pt7b, "Mittlere Ortszeit (MOZ):");
    showmsgXY(0, 125, 1, &FreeSans12pt7b, "Zeitgleichung :");
    showmsgXY(0, 150, 1, &FreeSans12pt7b, "Sonnenaufgang :");
    //  tft.setTextColor(RED, GREY);
    showmsgXY(0, 175, 1, &FreeSans12pt7b, "Sonnenuntergang");
    showmsgXY(0, 200, 1, &FreeSans12pt7b, "Tageslaenge:");
    showmsgXY(0, 225, 1, &FreeSans12pt7b, "Deklination:");
    showmsgXY(0, 250, 1, &FreeSans12pt7b, "Azimut :");
    showmsgXY(0, 275, 1, &FreeSans12pt7b, "Elevation :");
    showmsgXY(0, 300, 1, &FreeSans12pt7b, "Refraktion :");
    // showmsgXY(280, 50, 1, &FreeSans12pt7b, "10:58:12 h");
    // showmsgXY(280, 50, 1, &FreeSans12pt7b, ID);
}

// ==================================================================================================

void loop() {

    //================       Begin read incoming string     ===============

    // LoopCount++;
    // LoopPos++;
    // Serial.print("LoopCnt = ");
    // Serial.println(LoopCount); // nur für debuggging flow control
    //   Serial.print(" , Looppos = ");

    //   Serial.println( LoopPos);            // nur für debuggging flow
    //   control

    j = 0;
    GotString = false;
    getMSG = false;
    stringLength = 0;
    const unsigned int MaxMessageLength = 20;

    void loop() {
        // Check to see if anything is available in the serial receive buffer
        while(HwSerial.available() > 0) {
            // Create a place to hold the incoming message
            static char message[MaxMessageLength];
            static unsigned int message_pos = 0;

            // Read the next available byte in the serial receive buffer
            char inByte = HwSerial.read();

            // Message coming in (check not terminating character)
            if(inByte != '\n' && (message_pos < MaxMessageLength - 1)) {
                message[message_pos] = inByte;
                message_pos++;
            } else { // Full message received (it is a new line chr)
                // Add null character to string
                message[message_pos] = '\0'; // Generiert Extra Zeile
                // Print the message
                Serial.println(message);
                // Reset for the next message
                message_pos = 0;
            }
        }

        /*
            while(HwSerial.available()) {
                InChar = HwSerial.read();
                InString[j] = InChar;
                j++;
            }


            if(InString[0] == 'z') {
                Serial.println("InString = True");
                Serial.println(InString);
                // Proceed
            } else {
                Serial.println("InString = False"); // nur für debuggging flow
           control InString[j] = InChar; goto blabla;
            }
        // */
        // msgSelect = InString[1];
        // Serial.print("MSG = ");
        // Serial.println(msgSelect);

        // // Calculate leghth message

        // if(InString[2] == '-') {
        //     Temp0 = 0;
        // } else {
        //     Temp0 = InString[2] - 48;
        // }
        // Temp1 = InString[3] - 48;
        // stringLength = Temp0 * 10 + Temp1;

        // Serial.print("StringLength  = ");
        // Serial.println(stringLength);

        // j = 0;
        // for(int i = 0; i < (stringLength + 4); i++) {
        //     if(i > 3) {
        //         InString2[j] = InString[i];
        //         j++;
        //     }
        // }

        // InString = "";                // clear string

        Serial.println("Test");

        Serial.println();
        Serial.print("String = ");
        Serial.println(InString2); // InString2[2]

        count++;

        if(count == 2) {
            count = 0;
            if(msgSelect == 'a') {
                tft.writeFillRect(279, 0, 279, 26, BLACK);
            }
            if(msgSelect == 'b') {
                tft.writeFillRect(279, 25, 279, 56, BLACK);
            }
            // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
            delay(200); // slow down the program
        }

        // goto blabla;
        // tft.setCursor(x,y); //setCursor(int16_t x, int16_t y)  // This code
        // sets the cursor position to of x and y

        //    a  b  c   d   e   f   g   h   i   j   k  l
        //   97 98 99 100 101 102 103 104 105 106 107 108

        switch(msgSelect) {
        case 'a': // Taste "a" = Tag Nr. des lfd. Jahres FMT : 31:01:2021
            tft.writeFillRect(279, 0, 279, 25, BLACK); // Clear date rectangle
            // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
            tft.setCursor(280, 25);
            tft.print(InString2);
            delay(500); // slow down the program
            // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  0");
            break;
        case 'b': // Taste "b" = MEZ Mittel Europ. Zeit
            tft.writeFillRect(279, 25, 279, 56, BLACK);
            tft.setCursor(280, 50);
            tft.print(InString2);
            delay(500); // slow down the program
            // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
            break;
        case 'c': // Taste "c" = WOZ Wahre Ortszeit
            tft.writeFillRect(279, 50, 279, 75, BLACK);
            showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  2");
            break;
        case 'd': // Taste "d" = Mittlere Ortszeit
            tft.writeFillRect(279, 50, 279, 75, BLACK);
            showmsgXY(280, 75, 1, &FreeSans12pt7b, "       ");
            break;
        default:
            // Tue etwas, im Defaultfall
            // Dieser Fall ist optional
            break; // Wird nicht benötigt, wenn Statement(s) vorhanden sind
        }          // end Case

        // InString = "";
        // InString = "" ;
        // InString2 = "" ;
        for(int k = 0; k < 20; k++) { InString[k] = (' '); }
        for(int m = 0; m < 20; m++) { InString2[m] = (' '); }
    blabla:

        HwSerial.clearReadError();
        delay(500);

        HwSerial.begin(115200);
        delay(500);

        Serial.print("String = ");
        Serial.print("====  ENDE LOOP ===  "); // InString2[2]
        Serial.println(LoopCount);
    }