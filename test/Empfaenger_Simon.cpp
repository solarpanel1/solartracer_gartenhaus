// Empfänger = Teensy Links
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>

#define HwSerial Serial4

void setup() {
    Serial.begin(115200);
    HwSerial.begin(115200);
    Serial.println("HwSerial begins to read...");
}
const unsigned int MaxMessageLength = 20;

void loop() {
    // Check to see if anything is available in the serial receive buffer
    while(HwSerial.available() > 0) {
        // Create a place to hold the incoming message
        static char message[MaxMessageLength];
        static unsigned int message_pos = 0;

        // Read the next available byte in the serial receive buffer
        char inByte = HwSerial.read();

        // Message coming in (check not terminating character)
        if(inByte != '\n' && (message_pos < MaxMessageLength - 1)) {
            message[message_pos] = inByte;
            message_pos++;
        } else { // Full message received (it is a new line chr)
            // Add null character to string
            message[message_pos] = '\0'; // Generiert Extra Zeile
            // Print the message
            Serial.println(message);
            // Reset for the next message
            message_pos = 0;
        }
    }
}