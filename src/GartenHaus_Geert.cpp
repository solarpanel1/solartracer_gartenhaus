// Gartenhaus


#include <Adafruit_GFX.h> // Core graphics library
#include <Arduino.h>
#include <MCUFRIEND_kbv.h> // Hardware-specific library
#include <SPI.h>
#include <Wire.h>

// #define HwSerial Serial4
MCUFRIEND_kbv tft;

#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSerif12pt7b.h>
#include <FreeDefaultFonts.h>

#define BLACK 0x0000
#define RED 0xF800
#define GREEN 0x07E0
#define WHITE 0xFFFF
#define GREY 0x8410
#define NAVY 0x000F
#define DARKGREEN 0x03E0
#define DARKCYAN 0x03EF
#define MAROON 0x7800
#define PURPLE 0x780F
#define OLIVE 0x7BE0
#define LIGHTGREY 0xC618
#define DARKGREY 0x7BEF
#define BLUE 0x001F
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define ORANGE 0xFD20
#define GREENYELLOW 0xAFE5
#define PINK 0xF81F

#define TESTPIN 32

// Defs for incoming String
byte Year;
byte Month;
byte Date;
byte DoW;
byte Hour;
byte Minute;
byte Second;

boolean mainStart = false;
boolean GotString = false;
boolean getMSG = false;

int stringLength = 0;

byte(Temp0);
byte(Temp1);
char InChar;
char InString[20];
char InString2[20];
char msgSelect;

String incomingString;
int j = 0;
int x = 0;
int count = 0;
int LoopCount = 0;
int LoopPos = 0;

const unsigned int MaxMessageLength = 20; // Wir in Serial.read verwendet

void showmsgXY(int x, int y, int sz, const GFXfont *f, const char *msg) {
    tft.setFont(f);
    tft.setCursor(x, y);
    tft.setTextColor(GREEN);
    tft.setTextSize(sz);
    tft.print(msg);
    delay(100);
}

void setup() {
    pinMode(TESTPIN, OUTPUT);
    Serial.begin(115200);
    Serial4.begin(115200);
    /***
    //    uint16_t ID = tft.readID(); // <- Wegen dieser Zeile funktioniert
    Serial Serial.print("ID = 0x"); Serial.println(ID, HEX);
        // if (ID == 0xEFEF) {
        //    ID = 0x9486;
        //    }
        tft.begin(ID);
        //  tft.fillScreen(BLACK);


        if ( ID == 0x9341 ) {
        Serial.println(F("Found ILI9341 LCD driver"));
    }
    ***/
    // read vom anderen Teensy nicht mehr

    // Serial.println("Example: Font_simple");
    // Serial.print("found ID = 0x");

    // if(ID == 0xD3D3)
    //    ID = 0x9481; // force ID if write-only display
    uint16_t ID = tft.readID();

    Serial.println("Example: Font_simple");
    Serial.print("found ID = 0x");
    Serial.println(ID, HEX);
    if(ID == 0xD3D3) {
        ID = 0x9481; // force ID if write-only display
    }

//    while(Serial.available() == 0) {}

    // tft.begin(ID);
    // tft.reset();

    tft.setRotation(1);
    tft.fillScreen(BLACK);

    showmsgXY(0, 25, 1, &FreeSans12pt7b, "Tag Nr. des lfd. Jahres:");
    // showmsgXY(0, 50, 1, &FreeSans12pt7b, "Mitteleurop.Zeit  (MEZ):");
    // showmsgXY(0, 75, 1, &FreeSans12pt7b, "Wahre Ortszeit  (WOZ):");
    // showmsgXY(0, 100, 1, &FreeSans12pt7b, "Mittlere Ortszeit (MOZ):");
    // showmsgXY(0, 125, 1, &FreeSans12pt7b, "Zeitgleichung :");
    // showmsgXY(0, 150, 1, &FreeSans12pt7b, "Sonnenaufgang :");
    // //  tft.setTextColor(RED, GREY);
    // showmsgXY(0, 175, 1, &FreeSans12pt7b, "Sonnenuntergang");
    // showmsgXY(0, 200, 1, &FreeSans12pt7b, "Tageslaenge:");
    // showmsgXY(0, 225, 1, &FreeSans12pt7b, "Deklination:");
    // showmsgXY(0, 250, 1, &FreeSans12pt7b, "Azimut :");
    // showmsgXY(0, 275, 1, &FreeSans12pt7b, "Elevation :");
    // showmsgXY(0, 300, 1, &FreeSans12pt7b, "Refraktion :");
}

void loop() {

    /*
    static int i = 0 ;
        static char message[MaxMessageLength];
        static unsigned int message_pos = 0;

        while (HwSerial.available()) {   //receive reply into buffer via
    interrupts char c = HwSerial.read(); message[i++] = c; if (c == '\n' || i >
    98) { //process buffer when full sentence message[i] = '\0';
        //        if (parse) {           //only display when pointer is red
        //            tft.setCursor(0, 0);
        //            tft.fillRect(0, 0, tft.width(), 16, TFT_BLACK);
        //            tft.print(message_pos);
        //        }

                i = 0;
            }
        }
                    Serial.println(message);

    }
    */

    // Check to see if anything is available in the serial receive buffer
    while(Serial4.available() > 0) {
        // Create a place to hold the incoming message
        static char message[MaxMessageLength];
        static unsigned int message_pos = 0;

        // Read the next available byte in the serial receive buffer
        char inByte = Serial4.read();

        // Message coming in (check not terminating character)
        if(inByte != '\n' && (message_pos < MaxMessageLength - 1)) {
            message[message_pos] = inByte;
            message_pos++;
        } else { // Full message received (it is a new line chr)
            // Add null character to string
            message[message_pos] = '\0'; // Generiert Extra Zeile
            // Print the message
            Serial.println(message);
            // Reset for the next message
            message_pos = 0;
        }
    }
}

// Serial.println(InString2); // InString2[2]

// count++;

// if(count == 2) {
//     count = 0;
//     if(msgSelect == 'a') {
//         tft.writeFillRect(279, 0, 279, 26, BLACK);
//     }
//     if(msgSelect == 'b') {
//         tft.writeFillRect(279, 25, 279, 56, BLACK);
//     }
//     // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
//     delay(200); // slow down the program
// }

// switch(msgSelect) {
// case 'a': // Taste "a" = Tag Nr. des lfd. Jahres FMT : 31:01:2021
//     tft.writeFillRect(279, 0, 279, 25, BLACK); // Clear date rectangle
//     // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
//     tft.setCursor(280, 25);
//     tft.print(InString2);
//     delay(500); // slow down the program
//     // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  0");
//     break;
// case 'b': // Taste "b" = MEZ Mittel Europ. Zeit
//     tft.writeFillRect(279, 25, 279, 56, BLACK);
//     tft.setCursor(280, 50);
//     tft.print(InString2);
//     delay(500); // slow down the program
//     // showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  1");
//     break;
// case 'c': // Taste "c" = WOZ Wahre Ortszeit
//     tft.writeFillRect(279, 50, 279, 75, BLACK);
//     showmsgXY(280, 75, 1, &FreeSans12pt7b, "read  2");
//     break;
// case 'd': // Taste "d" = Mittlere Ortszeit
//     tft.writeFillRect(279, 50, 279, 75, BLACK);
//     showmsgXY(280, 75, 1, &FreeSans12pt7b, "       ");
//     break;
// default:
//     // Tue etwas, im Defaultfall
//     // Dieser Fall ist optional
//     break; // Wird nicht benötigt, wenn Statement(s) vorhanden sind
// }          // end Case

// for(int k = 0; k < 20; k++) { InString[k] = (' '); }
// for(int m = 0; m < 20; m++) { InString2[m] = (' '); }
