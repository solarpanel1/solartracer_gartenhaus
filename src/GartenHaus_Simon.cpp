// Gartenhaus
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>

// TFT
#include <Adafruit_GFX.h> // Core graphics library
// https://github.com/prenticedavid/MCUFRIEND_kbv
#include <MCUFRIEND_kbv.h> // Hardware-specific library
// #include <ili9341_t3n_font_Arial.h>
// #include <ili9341_t3n_font_ArialBold.h>
// #include <ili9341_t3n_font_ComicSansMS.h>
// #include <ili9341_t3n_font_OpenSans.h>
#include <myFonts/font_Michroma.h>
#include <myFonts/font_Michroma.c>
#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSerif12pt7b.h>
#include <FreeDefaultFonts.h>
MCUFRIEND_kbv tft;
// Startkoordinaten
int x1 = 20;
int x2 = 120;
int y = 20;

#define BLACK 0x0000
#define RED 0xF800
#define GREEN 0x07E0
#define WHITE 0xFFFF
#define GREY 0x8410
#define NAVY 0x000F
#define DARKGREEN 0x03E0
#define DARKCYAN 0x03EF
#define MAROON 0x7800
#define PURPLE 0x780F
#define OLIVE 0x7BE0
#define LIGHTGREY 0xC618
#define DARKGREY 0x7BEF
#define BLUE 0x001F
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define ORANGE 0xFD20
#define GREENYELLOW 0xAFE5
#define PINK 0xF81F

// LEDs
#define LED4_GN 29       // Links unten
#define LED5_RD 30       // Links oben
#define LED6_YG 31       // Rechts unten
#define LED7_BL 32       // Rechts oben
#define PIN_EN_RS3485 41 // DE für RS3485

#define HwSerial Serial4 // RS3485 Chip

// Defs for incoming String
byte Year;
byte Month;
byte Date;
byte DoW;
byte Hour;
byte Minute;
byte Second;

boolean mainStart = false;
boolean GotString = false;
boolean getMSG = false;

int stringLength = 0;

byte(Temp0);
byte(Temp1);
char InChar;
char InString[20];
char InString2[20];
char msgSelect;

String incomingString;
int j = 0;
int x = 0;
int count = 0;
int LoopCount = 0;
int LoopPos = 0;

const unsigned int MaxMessageLength = 80; // Wird in Serial.read verwendet

void showmsgXY(int x, int y, int sz, const GFXfont *f, const char *msg) {
    int16_t x1, y1;
    uint16_t wid, ht;
    tft.drawFastHLine(0, y, tft.width(), WHITE);
    tft.setFont(f);
    tft.setCursor(x, y);
    tft.setTextColor(GREEN);
    tft.setTextSize(sz);
    tft.print(msg);
    delay(1000);
}

void setup() {
    Serial.begin(115200);
    HwSerial.begin(115200);
    delay(1000);

    // LED init
    pinMode(LED4_GN, OUTPUT);
    pinMode(LED5_RD, OUTPUT);
    pinMode(LED6_YG, OUTPUT);
    pinMode(LED7_BL, OUTPUT);
    pinMode(PIN_EN_RS3485, OUTPUT);
    digitalWrite(LED4_GN, HIGH);
    digitalWrite(LED5_RD, HIGH);
    digitalWrite(LED6_YG, HIGH);
    digitalWrite(LED7_BL, HIGH);
    digitalWrite(PIN_EN_RS3485, LOW);

    while(!Serial && millis() < 8000) { ; }
    // TFT init
    // http:  // www.barth-dev.de/online/rgb565-color-picker/
    // https: // github.com/prenticedavid/MCUFRIEND_kbv
    // uint16_t ID = tft.readID();
    // tft.begin(ID);
    // tft.setRotation(0);
    // uint16_t w = tft.width();  // int16_t width(void);
    // uint16_t h = tft.height(); // int16_t height(void);
    // Serial.print(w);
    // Serial.print(" ");
    // Serial.println(h);
    // Serial.println("Example: Font_simple");
    // Serial.print("found ID = 0x");
    // Serial.println(ID, HEX);

    digitalWrite(LED4_GN, LOW);
    digitalWrite(LED5_RD, LOW);
    digitalWrite(LED6_YG, LOW);
    digitalWrite(LED7_BL, LOW);
    digitalWrite(PIN_EN_RS3485, LOW);
}

void loop() {
    // tft.fillScreen(BLACK);
    // tft.setTextColor(WHITE);
    // Check to see if anything is available in the serial receive buffer
    while(HwSerial.available() > 0) {
        // Create a place to hold the incoming message
        static char message[MaxMessageLength];
        static unsigned int message_pos = 0;

        // Read the next available byte in the serial receive buffer
        char inByte = HwSerial.read();

        // Message coming in (check not terminating character)
        if(inByte != '\n' && (message_pos < MaxMessageLength - 1)) {
            message[message_pos] = inByte;
            message_pos++;
        } else { // Full message received (it is a new line chr)
            // Add null character to string
            message[message_pos] = '\0'; // Generiert Extra Zeile
            // Print the message
            Serial.println(message);
            // tft.setCursor(x1, y);
            // tft.print("Datum");
            // tft.setCursor(x2, y);
            // showmsgXY(x1, y, 1, NULL, message);
            // Reset for the next message
            message_pos = 0;
        }
    }
    // Serial.println("Test, if Loop works");
}